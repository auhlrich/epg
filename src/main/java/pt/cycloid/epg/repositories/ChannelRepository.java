package pt.cycloid.epg.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pt.cycloid.epg.domains.Channel;

@Repository
public interface ChannelRepository extends JpaRepository<Channel, String> {
}
