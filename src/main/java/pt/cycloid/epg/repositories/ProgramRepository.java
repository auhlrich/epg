package pt.cycloid.epg.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pt.cycloid.epg.domains.Program;

@Repository
public interface ProgramRepository extends JpaRepository<Program, String> {
}
