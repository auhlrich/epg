package pt.cycloid.epg.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.cycloid.epg.domains.Channel;
import pt.cycloid.epg.repositories.ChannelRepository;
import pt.cycloid.epg.services.ChannelService;

import java.util.List;

@Service
public class ChannelServiceImpl implements ChannelService {

    @Autowired
    ChannelRepository channelRepository;
    @Override
    public Channel insert(Channel channel) {
        return channelRepository.save(channel);
    }

    @Override
    public List<Channel> findAll() {
        return channelRepository.findAll();
    }
}
