package pt.cycloid.epg.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pt.cycloid.epg.domains.Channel;
import pt.cycloid.epg.domains.Program;
import pt.cycloid.epg.domains.ProgramRequest;
import pt.cycloid.epg.exceptions.ObjectNotFoundException;
import pt.cycloid.epg.repositories.ChannelRepository;
import pt.cycloid.epg.repositories.ProgramRepository;
import pt.cycloid.epg.services.ProgramService;

import java.util.List;

@Service
public class ProgramServiceImpl implements ProgramService {

    @Autowired
    ProgramRepository programRepository;
    @Autowired
    ChannelRepository channelRepository;

    @Override
    public Program insert(ProgramRequest programRequest) {
        Channel channel = getChannelFromId(programRequest.getChannelId());

        Program program = buildProgramFromRequest(programRequest, channel);
        return programRepository.save(program);
    }

    private Channel getChannelFromId(String channelId) {
        return channelRepository.findById(channelId).orElseThrow(() -> new ObjectNotFoundException(
                "Channel ["+channelId+"] not found"
        ));
    }

    private Program buildProgramFromRequest(ProgramRequest programRequest, Channel channel) {
        return Program.builder()
                .id(programRequest.getId())
                .channel(channel)
                .description(programRequest.getDescription())
                .title(programRequest.getTitle())
                .imageUrl(programRequest.getImageUrl())
                .startTime(programRequest.getStartTime())
                .endTime(programRequest.getEndTime())
                .build();
    }

    @Override
    public List<Program> findAll() {
        return programRepository
                .findAll();
    }

    @Override
    public Program findById(String id) {
        return getProgram(id);
    }

    private Program getProgram(String id) {
        return getProgram(id);
    }

    @Override
    public void delete(String id) {
        getProgram(id);
        programRepository.deleteById(id);
    }

    @Override
    public Program update(String id,ProgramRequest programRequest) {
        Program program = getProgram(id);
        program.setChannel(getChannelFromId(programRequest.getChannelId()));
        program.setDescription(programRequest.getDescription());
        program.setImageUrl(programRequest.getImageUrl());
        program.setTitle(programRequest.getTitle());
        program.setStartTime(programRequest.getStartTime());
        program.setEndTime(programRequest.getEndTime());
        return null;
    }
}
