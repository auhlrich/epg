package pt.cycloid.epg.services;

import pt.cycloid.epg.domains.Program;
import pt.cycloid.epg.domains.ProgramRequest;

import java.util.List;

public interface ProgramService {
    Program insert(ProgramRequest programRequest);
    List<Program> findAll();
    Program findById(String id);
    void delete(String id);
    Program update(String id,ProgramRequest programRequest);
}
