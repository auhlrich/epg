package pt.cycloid.epg.services;

import pt.cycloid.epg.domains.Channel;

import java.util.List;

public interface ChannelService {
    Channel insert(Channel channel);
    List<Channel> findAll();
}
