package pt.cycloid.epg.resources;

import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pt.cycloid.epg.domains.Channel;
import pt.cycloid.epg.services.ChannelService;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(value = "/channels")
@AllArgsConstructor
public class ChannelResource {

    private ChannelService channelService;

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Channel>> list() {

        return ResponseEntity.ok(channelService.findAll());
    }

    @RequestMapping(method = RequestMethod.POST)
    @Transactional
    public ResponseEntity<Void> insert(@Valid @RequestBody Channel channel) {

        Channel inserted = channelService.insert(channel);

        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}").buildAndExpand(inserted.getId()).toUri();
        return ResponseEntity.created(uri).build();
    }
}
