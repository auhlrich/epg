package pt.cycloid.epg.resources;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import pt.cycloid.epg.domains.Channel;
import pt.cycloid.epg.domains.Program;
import pt.cycloid.epg.domains.ProgramRequest;
import pt.cycloid.epg.services.ChannelService;
import pt.cycloid.epg.services.ProgramService;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;

@RestController
@RequestMapping(value = "/programs")
@AllArgsConstructor
public class ProgramResource {

    private ProgramService programService;

    @RequestMapping(value="/{id}",method = RequestMethod.GET)
    public ResponseEntity<Program> getDetail(@PathVariable String id) {
        return ResponseEntity.ok(programService.findById(id));
    }

    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<List<Program>> list() {
        return ResponseEntity.ok(programService.findAll());
    }

    @RequestMapping(method = RequestMethod.POST)
    @Transactional
    public ResponseEntity<Void> insert(@Valid @RequestBody ProgramRequest program) {

        Program inserted = programService.insert(program);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest()
                .path("/{id}").buildAndExpand(inserted.getId()).toUri();
        return ResponseEntity.created(uri).build();
    }

    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    @Transactional
    public ResponseEntity<Void> delete(@PathVariable String id) {
        programService.delete(id);
        return ResponseEntity.noContent().build();
    }
    @RequestMapping(value="/{id}", method=RequestMethod.PUT)
    @Transactional
    public ResponseEntity<Void> delete(@PathVariable String id, @Valid @RequestBody ProgramRequest program) {
        programService.update(id, program);
        return ResponseEntity.noContent().build();
    }
}
