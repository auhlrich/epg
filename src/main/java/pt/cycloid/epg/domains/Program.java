package pt.cycloid.epg.domains;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name="program", schema= "EPG")
public class Program {
    @Id
    @Column(length = 10)
    @NotBlank(message = "Id is mandatory")
    private String id;

    @ManyToOne(fetch = FetchType.EAGER)
    private Channel channel;

    private String imageUrl;
    private String title;
    private String description;

    private LocalDateTime startTime;
    private LocalDateTime endTime;
}
