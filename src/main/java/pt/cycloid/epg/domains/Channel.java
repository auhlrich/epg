package pt.cycloid.epg.domains;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Table(name="channel", schema= "EPG")
public class Channel {
    @Id
    @Column(length = 10)
    @NotBlank(message = "Id is mandatory")
    private String id;

    private String name;

    private Long position;

    private String category;
}
