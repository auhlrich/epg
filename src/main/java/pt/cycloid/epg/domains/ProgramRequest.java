package pt.cycloid.epg.domains;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProgramRequest {
    @Id
    @Column(length = 10)
    private String id;

    private String channelId;

    private String imageUrl;
    private String title;
    private String description;

    private LocalDateTime startTime;
    private LocalDateTime endTime;
}
